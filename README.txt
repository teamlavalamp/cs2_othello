Riley - 
Reasearched game, tested strategies
Wrote code base for recursion of boardstates
Wrote code base for basic moving and evaluation of moves
Eli - 
Wrote code base for basic moving and evaluation of moves
Reasearched game, tested strategies
Wrote code for heuristics of board evaluation

Our player uses several strategies. It evaluates the values of positions on the board with the value heuristic from the Windows XP reversi implementation. It recurses one move in, checking the value of a move versus the values of moves the opponent could make, and evaluates those moves with tempo considerations with different weights for black and white. Ideas that went unimplemented to time constraints were a heurisitic measure of the number of stable pieces a side has, along with an alpha beta pruning of a deeper recursive enumeration of moves.
