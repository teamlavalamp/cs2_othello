#include "exampleplayer.h"

// helper functions
int Value(int x, int y) {
     if((x == 0 || x == 7) && (y == 0 || y == 7)) {
         return 99;
     }
     if((x == 1 || x == 6) && (y == 0 || y == 7)) {
         return -8;
     }
     if((x == 2 || x == 5) && (y == 0 || y == 7)) {
         return 8;
     }
     if((x == 3 || x == 4) && (y == 0 || y == 7)) {
         return 6;
     }
     if((x == 1 || x == 6) && (y == 1 || y == 6)) {
         return -24;
     }
     if((x == 2 || x == 5) && (y == 1 || y == 6)) {
         return -4;
     }
     if((x == 3 || x == 4) && (y == 1 || y == 6)) {
         return -3;
     }
     if((x == 2 || x == 5) && (y == 2 || y == 5)) {
         return 7;
     }
     if((x == 3 || x == 4) && (y == 2 || y == 5)) {
         return 4;
     }
     if((x == 3 || x == 4) && (y == 3 || y == 4)) {
         return 0;
     }
     if((x == 0 || x == 7) && (y == 1 || y == 6)) {
         return -8;
     }
     if((x == 0 || x == 7) && (y == 2 || y == 5)) {
         return 8;
     }
     if((x == 0 || x == 7) && (y == 3 || y == 4)) {
         return 6;
     }
     if((x == 1 || x == 6) && (y == 2 || y == 5)) {
         return -4;
     }
     if((x == 1 || x == 6) && (y == 3 || y == 4)) {
         return -3;
     }
     if((x == 2 || x == 5) && (y == 3 || y == 4)) {
         return 4;
     }
     return 0;
 }

int ExamplePlayer::boardValue(Board board) {
    int control = 0;
    int mobility = board.mobility(ourSide) - board.mobility(theirSide);
    int frontier = board.frontier(theirSide) - board.frontier(ourSide);
    int stability = 0; // Do something with me!
    vector<Move> moves = board.moves(theirSide);
    for(int i = 0; i < moves.size(); i++){
        control += Value(moves.at(i).x, moves.at(i).y);
    }
    if(ourSide == WHITE){
         return -10 * control + 0 * mobility +  2 * frontier + stability;
    }
    else {
        return -10 * control + 5 * mobility +  -3 * frontier + stability;
    }
}


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    // Init provided board class, stores which side is our side in ourSide
    ourSide = side;
    if(ourSide == WHITE) {
        theirSide = BLACK;
    }
    else {
        theirSide = WHITE;
    }
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    // Process the opponents move
    board.doMove(opponentsMove, theirSide);

    // Init necessary stuff
    vector<Move> openMoves = board.moves(ourSide);
    vector<int> boardValues;
    int highVal = -35535;
    Move bestMove = Move(-1, -1);

    if(board.hasMoves(ourSide)) {
        vector<Board> nextBoards = enumNextBoards(openMoves, msLeft);
        
        // Choose a move
        for(int i = 0; i < nextBoards.size(); i++) {
            if(boardValue(nextBoards.at(i)) > highVal) {
                highVal = boardValue(nextBoards.at(i));
                bestMove = openMoves.at(i);
            }
        }

        // Update board with our move
        board.doMove(&bestMove, ourSide);

        return new Move(bestMove.x, bestMove.y);
    }
    else {
        return NULL;
    }
}

vector<Board> ExamplePlayer::enumNextBoards(vector<Move> ourMoves, int msLeft){
    vector<Board> nextBoards;
    for(int i = 0; i < ourMoves.size(); i++){
        Board nextBoard = *board.copy();
        nextBoard.doMove(&ourMoves.at(i), ourSide);
        nextBoards.push_back(nextBoard);
    }
    return nextBoards;
}













    
