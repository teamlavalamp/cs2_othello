#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"

using namespace std;

class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    Side ourSide;
    Side theirSide;
    Board board;
    Move *doMove(Move *opponentsMove, int msLeft);
    vector<Board> enumNextBoards(vector<Move> ourMoves, int msLeft);
    int boardValue(Board board);
};

#endif
